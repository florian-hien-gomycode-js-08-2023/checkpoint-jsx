const productList = {
  name: "Rottweiler Puppy",
  image: require("./IMG-1.jpg"),
  description: "Rottweiler puppies are known for their strong and loyal nature: \nThey exhibit a strong bond with their owners and are fiercely protective.",
  price: 500,
};
export default productList;
