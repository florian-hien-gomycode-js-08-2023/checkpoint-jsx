import Name from "./components/Name";
import Description from "./components/Description";
import Image from "./components/Image";
import Price from "./components/Price";

import { Button, Card, Container } from "react-bootstrap";

function App() {
  return (
    <div className="App">
    <Card style={{ width: '18rem' }}>
    <Image></Image>
      <Card.Body>
        <Card.Title><Name></Name></Card.Title>
        <Card.Text>Prix : <span className="fw-bold">$<Price></Price></span></Card.Text>
        <Card.Text> <Description></Description></Card.Text>
        <Container><Button>Add To Card</Button></Container>
      </Card.Body>
    </Card>
    </div>
  );
}

export default App;
